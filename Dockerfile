FROM python:3
COPY riptester /app
WORKDIR /
CMD ["python", "-OOm", "app"]
