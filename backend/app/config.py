import os
from dotenv import dotenv_values

config = {
    **dotenv_values(".env"),  # load shared development variables
    **dotenv_values(".env.secret"),  # load sensitive variables
    **os.environ,  # override loaded values with environment variables
}

STOP_TIMEOUT = int(config["STOP_TIMEOUT"])
MEMORY_LIMIT = int(config["MEMORY_LIMIT"])
PIDS_LIMIT = int(config["PIDS_LIMIT"])
CPU_PERIOD = int(config["CPU_PERIOD"])
CPU_QUOTA = int(config["CPU_QUOTA"])
EXEC_TIME = int(config["EXEC_TIME"])
MAX_CODE_LENGTH = int(config["MAX_CODE_LENGTH"])
MAX_STDIN_LENGTH = int(config["MAX_STDIN_LENGTH"])
MAX_STDOUT_LENGTH = int(config["MAX_STDOUT_LENGTH"])
MAX_STDERR_LENGTH = int(config["MAX_STDERR_LENGTH"])
TMPFS_SIZE = int(config["TMPFS_SIZE"])
MAX_ENVIRON_LENGTH = int(config["MAX_ENVIRON_LENGTH"])
MAX_ENVIRON_ENTRY_LENGTH = int(config["MAX_ENVIRON_ENTRY_LENGTH"])

# TODO https://docs.aiohttp.org/en/stable/web_advanced.html#data-sharing-aka-no-singletons-please