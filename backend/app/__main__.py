from aiohttp import web

from .api import app


web.run_app(app)
