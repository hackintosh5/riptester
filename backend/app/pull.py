async def pull_image(docker, image_name):
    await docker.images.pull(image_name)
