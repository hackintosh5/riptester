import asyncio
import logging
import struct
import typing

logger = logging.getLogger(__file__)

#
# class Client:
#     _readers: dict[int, asyncio.Queue]
#
#     def __init__(self):
#         self._readers = {}
#         self._transport = None
#         self._connected = False
#
#     def connection_made(self, transport: asyncio.transports.BaseTransport) -> None:
#         self._writer =
#         if not self._connected:
#             self._writer.write(b"\0\0\0\0")
#         self._connected = True
#
#     def data_received(self, data: bytes) -> None:
#         session, = struct.unpack_from("!I", data)
#         try:
#             reader = self._readers[session]
#         except KeyError:
#             logger.warning("Gatekeeper responded to unknown session %d with %r", session, data.hex())
#             return
#         reader.put_nowait(data[:4:])
#
#     def start_session(self):
#         session = 1
#         while session in self._readers:
#             session += 1
#         self._transport.write(struct.pack("!I", session))
#         reader = asyncio.Queue()
#         self._readers[session] = reader
#         return reader, self._transport, session


class Client:
    _readers: dict[int, asyncio.Queue]
    _reader: typing.Optional[asyncio.StreamReader]
    _writer: typing.Optional[asyncio.StreamWriter]

    def __init__(self):
        self._readers = {}
        self._reader = None
        self._writer = None

    async def init(self):
        self._reader, self._writer = await asyncio.open_connection('127.0.0.1', 8081)
        asyncio.create_task(self._pump())

    async def _pump(self):
        while True:
            session, length = struct.unpack("!II", await self._reader.readexactly(8))
            try:
                session_reader = self._readers[session]
            except KeyError:
                logger.warning("Client responded to unknown session %d with length %d", session, length)
                continue
            session_reader.put_nowait(await self._reader.readexactly(length))

    def start_session(self):
        session = 1
        while session in self._readers:
            session += 1
        self._writer.write(struct.pack("!III", 0, 4, session))
        reader = asyncio.Queue()
        self._readers[session] = reader
        return reader, self._writer, session


def write_var_string(data: bytes):
    return struct.pack("!I", len(data)) + data


def write_var_string_array(data: list[bytes]):
    return struct.pack("!I", len(data)) + b"".join(write_var_string(elem) for elem in data)


def write_int(data: int):
    return struct.pack("!I", data)
