from aiohttp import web

@web.middleware
async def auth_middleware(request: web.Request, handler: web.RequestHandler):
    if request.headers["X-Api-Token"] != "TOKEN":
        return web.json_response({}, status=200) # TODO
