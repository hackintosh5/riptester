import asyncio
import struct
from collections import AsyncIterable, Awaitable
from typing import Callable

import aiohttp
from aiohttp import web

from . import config, errors
from .errors import ErrorCode
from .gatekeeper_client import Client
from .languages import LANGUAGES_MAPPING
from .run import run_code, ProcessResult
from .utils import async_iterable

CLIENT = Client()


async def get_landing(request: web.Request):
    return web.Response(text="Riptester backend is alive!")


async def _common_exec_language(
        language: str,
        code: AsyncIterable[bytes],
        stdin: AsyncIterable[bytes],
        environment: list[bytes],
        callback: Callable[[int, bytes], Awaitable[bool]]
):
    language = LANGUAGES_MAPPING[language]
    runner = run_code(CLIENT, language, code, stdin, environment)
    stop = None
    while message := await runner.asend(stop):
        if isinstance(message, ProcessResult):
            return message
        stop = await callback(message.stream, message.data)


async def post_exec_language(request: web.Request):
    language = request.match_info["language"]
    params = await request.post()
    code = params["code"].encode("latin-1")
    stdin = params["stdin"].encode("latin-1")
    environ = [entry.encode("latin-1") for entry in params["environ"].split("\0") if entry]
    if len(code) > config.MAX_CODE_LENGTH:
        raise errors.CodeTooLong(request)
    if len(stdin) > config.MAX_STDIN_LENGTH:
        raise errors.StdinTooLong(request)
    if len(environ) > config.MAX_ENVIRON_LENGTH:
        raise errors.EnvironTooLong(request)
    if any(len(entry) > config.MAX_ENVIRON_ENTRY_LENGTH for entry in environ):
        raise errors.EnvironEntryTooLong(request)
    if any(b"=" not in entry for entry in environ):
        raise errors.EnvironWithoutEquals(request)
    stdout = b""
    stderr = b""

    async def callback(stream, data):
        nonlocal stdout, stderr
        if stream == 1:
            stdout += data
            if len(stdout) > config.MAX_STDOUT_LENGTH:
                stdout = stdout[:config.MAX_STDOUT_LENGTH]
                return True
        elif stream == 2:
            stderr += data
            if len(stderr) > config.MAX_STDERR_LENGTH:
                stderr = stderr[:config.MAX_STDERR_LENGTH]
                return True
        else:
            assert False
        return False

    ret = await _common_exec_language(language, async_iterable((code,)), async_iterable((stdin,)), environ, callback)
    return web.json_response({
        "stdout": stdout.decode("latin-1"),
        "stderr": stderr.decode("latin-1"),
        "rc": ret.rc,
        "runtime": ret.runtime
    })


async def post_exec_ws_language(request: web.Request):
    language = request.match_info["language"]
    ws = web.WebSocketResponse()
    request.app["websockets"].add(ws)
    await ws.prepare(request)
    code_queue = asyncio.Queue()
    stdin_queue = asyncio.Queue()
    environ = []

    # TODO (rate)limits on I/O

    async def code() -> AsyncIterable[bytes]:
        while True:
            yield code_queue.get()

    async def stdin() -> AsyncIterable[bytes]:
        while True:
            yield stdin_queue.get()

    async def callback(stream, data):
        await ws.send_bytes(bytes([stream]) + data)
        return False

    async def main_task():
        ret = await _common_exec_language(language, code(), stdin(), environ.copy(), callback)
        await ws.send_bytes(struct.pack(
            "!BBI",
            0,
            ret.rc,
            ret.runtime
        ))
        await ws.close()

    async for msg in ws:
        assert isinstance(msg, aiohttp.WSMessage)  # TODO pycharm bug
        if msg.type != aiohttp.WSMsgType.BINARY:
            await ws.close(code=aiohttp.WSCloseCode.UNSUPPORTED_DATA)
        if msg[0] == 0:
            await ws.close()
        elif msg[0] == 1:
            await code_queue.put(msg[1:])
        elif msg[0] == 2:
            await stdin_queue.put(msg[1:])
        elif msg[0] == 3:
            if len(environ) >= config.MAX_ENVIRON_LENGTH:
                # TODO limit environ length
                pass
            if len(msg) >= config.MAX_ENVIRON_ENTRY_LENGTH:
                # TODO limit environ entry length
                pass
            if b"=" not in msg[1:]:
                # TODO report that env is invalid
                continue
            environ.append(msg[1:])
        elif msg[0] == 4:
            asyncio.create_task(main_task())
        else:
            await ws.close(code=4000 + ErrorCode.PROTOCOL_ERROR)

    return ws
