import logging
import weakref

from aiohttp import web, WSCloseCode

from . import routes

logging.basicConfig(level=logging.DEBUG)

app = web.Application()

app["websockets"] = weakref.WeakSet()


async def on_shutdown(_):
    for ws in set(app["websockets"]):
        await ws.close(code=WSCloseCode.GOING_AWAY)
    # TODO stop all containers
app.on_shutdown.append(on_shutdown)


async def on_startup(_):
    await routes.CLIENT.init()
app.on_startup.append(on_startup)

app.add_routes([
    web.get("/", routes.get_landing),
    web.post("/v1/exec/{language}", routes.post_exec_language),
    web.post("/v1/exec/ws/{language}", routes.post_exec_ws_language),
])
