import asyncio
import logging

from . import server

logging.basicConfig(level=logging.DEBUG)

asyncio.run(server.main())
