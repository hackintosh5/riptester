from enum import IntEnum, unique


@unique
class ErrorCode(IntEnum):
    CODE_TOO_LONG = 0
    STDIN_TOO_LONG = 1
    PROTOCOL_ERROR = 2
    ENVIRON_TOO_LONG = 3
    ENVIRON_ENTRY_TOO_LONG = 4
    ENVIRON_ENTRY_MISSING_EQUALS = 5


class RiptesterError(RuntimeError):
    def __init__(self, data: dict):
        super().__init__(data.pop("message"))
        self.code = data.pop("code")
        self.data = data


class CodeTooLong(RiptesterError):
    pass


class StdinTooLong(RiptesterError):
    pass


class ProtocolError(RiptesterError):
    pass


class EnvironTooLong(RiptesterError):
    pass


class EnvironEntryTooLong(RiptesterError):
    pass


class EnvironEntryMissingEquals(RiptesterError):
    pass


def error_from_json(data: dict):
    if data["code"] == ErrorCode.CODE_TOO_LONG:
        return CodeTooLong(data)
    if data["code"] == ErrorCode.STDIN_TOO_LONG:
        return StdinTooLong(data)
    if data["code"] == ErrorCode.PROTOCOL_ERROR:
        return ProtocolError(data)
    if data["code"] == ErrorCode.ENVIRON_TOO_LONG:
        return EnvironTooLong(data)
    if data["code"] == ErrorCode.ENVIRON_ENTRY_TOO_LONG:
        return EnvironEntryTooLong(data)
    if data["code"] == ErrorCode.ENVIRON_ENTRY_MISSING_EQUALS:
        return EnvironEntryMissingEquals(data)
    else:
        return RiptesterError(data)
