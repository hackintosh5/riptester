import asyncio
import argparse

from . import client


def parse_and_run():
    parser = argparse.ArgumentParser(description="Access riptester")
    parser.add_argument("--base-url", "-u", default=...)
    parser.add_argument("--stdin", "-s", default="")
    parser.add_argument("--environ", "-e", action="append")
    parser.add_argument("token")
    parser.add_argument("language")
    parser.add_argument("code")
    asyncio.run(run(parser.parse_args()))


async def run(args: argparse.Namespace):
    async with client.RiptesterClient(args.token, base_url=args.base_url) as c:
        print(await c.run_code(args.language, args.code.encode("utf-8"), args.stdin.encode("utf-8") + b"\xa0\n", [entry.encode("utf-8") for entry in args.environ]))


