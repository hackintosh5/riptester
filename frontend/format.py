import signal


def escape(text):
    return text.replace("&", "&amp;").replace("<", "&lt;").encode(">", "&gt;")


def escape_bytes(text):
    return escape(text.decode("utf-8", "replace"))


def format_result(language, code, stdin, stdout, stderr, rc, short):
    ret = ""
    if not short:
        ret += "<b>Language</b>: <code>" + escape(language) + "</code>\n"
        if code:
            ret += "<b>Code</b>:\n<code>"
            ret += escape_bytes(code)
            ret += "</code>\n"
        if stdin:
            ret += "<b>Stdin</b>:\n<code>"
            ret += escape_bytes(stdin)
            ret += "</code>\n"
    if stdout:
        ret += "<b>Stdout</b>:\n<code>"
        ret += escape_bytes(stdout)
        ret += "</code>\n"
    if stderr:
        ret += "<b>Stderr</b>:\n<code>"
        ret += escape_bytes(stderr)
        ret += "</code>\n"
    if rc is not None:
        ret += f"<b>Exit code</b>: <code>{rc}</code> (<code>{signal.strsignal(rc - 128)}</code>)\n"
    return ret
